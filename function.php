<?php
class Koneksi
{
    var $host;
    var $username;
    var $pass;
    var $db;
    public function __construct($host, $username, $pass, $db)
    {
        $this->host = $host;
        $this->username = $username;
        $this->pass = $pass;
        $this->db = $db;
    }
    public function connect()
    {
        $conn = mysqli_connect($this->host, $this->username, $this->pass, $this->db);
        if ($conn) {
            return $conn;
        } else {
            echo "Connection Fail!!";
        }
    }
}
