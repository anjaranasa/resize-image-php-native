<?php
include "function.php";
$o = new Koneksi("localhost", "root", "mainbola123", "post");
$conn = $o->connect();

$nama   = $_FILES['file']['name'];
$size   = $_FILES['file']['size'];
$asal   = $_FILES['file']['tmp_name'];
$format = pathinfo($nama, PATHINFO_EXTENSION);
// print_r($nama);

// setup ukuran lebar gambar (width)
$width_size = 1600;
if ($format == "jpg" or $format == "jpeg") {
    move_uploaded_file($asal, "images/" . $nama);
    $asli    = "images/" . $nama;
    $nama_baru    = $nama;
    $gambar_asli    = imagecreatefromjpeg($asli);
    $lebar_asli      = imageSX($gambar_asli);
    $tinggi_asli     = imageSY($gambar_asli);
    $pembagi         = $lebar_asli / $width_size;
    $lebar_baru     = $lebar_asli / $pembagi;
    $tinggi_baru     = $tinggi_asli / $pembagi;

    $img = imagecreatetruecolor($lebar_baru, $tinggi_baru);
    imagecopyresampled($img, $gambar_asli, 0, 0, 0, 0, $lebar_baru, $tinggi_baru, $lebar_asli, $tinggi_asli);
    imagejpeg($img, $asli . $nama);
    imagedestroy($gambar_asli);
    imagedestroy($img);
    $sql = "insert into posts(judul,body) values('oke','$nama_baru')";
    $result = mysqli_query($conn, $sql);
    if ($result) {
        unlink("images/$nama");
        echo "oke";
    }
} else if ($format == "png") {
    move_uploaded_file($asal, "images/" . $nama);
    $asli = "images/" . $nama;
    $nama_baru    = $nama;
    $gambar_asli      = imagecreatefrompng($asli);
    $lebar_asli     = imageSX($gambar_asli);
    $tinggi_asli     = imageSY($gambar_asli);
    $pembagi         = $lebar_asli / $width_size;
    $lebar_baru     = $lebar_asli / $pembagi;
    $tinggi_baru     = $tinggi_asli / $pembagi;

    $img = imagecreatetruecolor($lebar_baru, $tinggi_baru);
    imagecopyresampled($img, $gambar_asli, 0, 0, 0, 0, $lebar_baru, $tinggi_baru, $lebar_asli, $tinggi_asli);
    imagejpeg($img, $asli . $nama);
    imagedestroy($gambar_asli);
    imagedestroy($img);
    $sql = "insert into posts(judul,body) values('oke','$nama_baru')";
    $result = mysqli_query($conn, $sql);
    if ($result) {
        unlink("images/$nama");

        echo "oke";
    }
}
